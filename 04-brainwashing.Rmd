# Lavaggio del cervello

Questa è la seconda ragione per cui cominciamo. Al fine di compredere appieno il lavaggio del cervello, dobbiamo prima esaminare i potenti effetti dello stimolo soprannaturale. I nostri cervelli semplicemente non sono preparati per la creazione di un ’harem online’ che ci permette di vedere più potenziali partner in quindici minuti di quanti ne abbiano potuti vedere i nostri antenati nell'arco di diverse generazioni.

Ci sono stati molti consigli sbagliati in passato, un esempio è che la masturbazione porta alla cecità. Ciò, insieme ad altre tattiche allarmistiche, è stata chiaramente un'esagerazione. Concezioni errate come queste sarebbero dovute essere chiarite dalla scienza. Ma il bambino è stato buttato via con l'acqua sporca; fin dai nostri primi anni le nostre menti subconsce sono bombardate da messaggi e immagini sessuali, riviste e pubblicità cariche di allusioni. Alcuni video pop sono estremamente suggestivi, ma non disperate, fatene un gioco di identificare quali componenti stanno usando - valore di shock, novità, colore, dimensione, tabù, nostalgia, ecc. Un tale gioco può anche essere insegnato ai preadolescenti in modo da educarli.

Al suo cuore, il messaggio è *"La cosa più preziosa su questa terra, il mio ultimo pensiero e azione, sarà l'orgasmo."* È un'esagerazione? Guardate qualsiasi trama televisiva o cinematografica e vedrete la confusione tra la parte sensoriale (tatto, odore, voce) e quella propagativa (orgasmica) del sesso. L'impatto di questo non si registra sul nostro conscio, ma il subconscio ha il tempo di assorbirlo.


## Ragionamento scientifico

C'è pubblicità dall'altra parte, la paura delle disfunzioni sessuali, la perdita di motivazione, preferire il porno virtuale a partner in carne ed ossa, YourBrainOnPorn e varie sottoculture di internet, ma questi movimenti non aiutano effettivamente le persone a smettere. A rigor di logica dovrebbero, ma il semplice fatto è che non lo fanno. Anche i rischi per la salute elencati dagli studi su YourBrainOnPorn non sono sufficienti a fermare un adolescente dal cominciare.

Ironicamente, la forza più potente in questa confusione è l'utente stesso. È una falsità che gli utenti siano persone deboli di volontà o fisicamente fragili. Bisogna essere fisicamente forti per affrontare una dipendenza dopo che si sa che esiste. Forse l'aspetto più doloroso è che si considerano perdenti senza successo e introversi insopportabili. È probabile che un amico potrebbe essere più interessante dal punto di vista personale se non si perdesse nella ricerca di piaceri.

![The Pornography Trap](images/trap.png)

## Problemi nell'usare la forza di volontà

Gli utenti che smettono con il metodo della forza di volontà danno la colpa alla loro mancanza di forza di volontà e rovinano la loro pace e felicità. Una cosa è fallire nell'autodisciplina e un'altra è il disprezzo di se stessi. Dopotutto, non c'è nessuna legge che ti impone di essere sempre dell'umore giusto prima del sesso, adeguatamente eccitato e capace di soddisfare il tuo partner. Stiamo lavorando su una dipendenza, non su un'abitudine e in nessun momento si considera l'idea di mollare un'abitudine come il golf, ma fare lo stesso con la dipendenza dal porno è normalizzato, perché?

L'esposizione costante a uno stimolo supernormale ricabla il tuo cervello, quindi costruire una resistenza a questo lavaggio del cervello è fondamentale, come se comprassi un'auto da un rivenditore di auto usate -- annuendo educatamente ma non credendo a una parola di quello che dice. Quindi non credere di dover fare più sesso possibile, tutto di tipo eccezionale, usando i porno in sua assenza.

Non fare nemmeno il gioco del porno sicuro, il tuo piccolo mostro ha inventato quel gioco per ingannarti. Il porno amatoriale è certificato da qualche autorità? I siti porno raccolgono dati dai loro utenti e li usano per soddisfare le loro esigenze, se vedono un aumento in una certa categoria si concentrano su di essa e fanno uscire i contenuti il prima possibile. Non fatevi ingannare da intenti educativi o da clip ’sicure’ per il mercato femminile. Inizia a chiederti: *"Perché lo sto facendo? Ne ho davvero bisogno?"*

**No, certo che no!**

La maggior parte degli utenti giurano che guardano solo porno statico e soft e quindi stanno bene, quando in realtà stanno tirando il guinzaglio, lottando con la loro forza di volontà per resistere alle tentazioni. Se fatto troppo spesso e per troppo tempo, questo esaurisce la loro forza di volontà in modo considerevole e cominciano a fallire in altri progetti di vita in cui la forza di volontà ha un grande valore, come l'esercizio fisico, la dieta, ecc. Il fallimento in queste aree li fa sentire miserabili e colpevoli, e così ricadono nella pornografia. Se questo non viene fatto, sfogheranno la loro rabbia e depressione sui loro cari.

Una volta che si diventa dipendenti dal porno su internet, il lavaggio del cervello aumenta. La tua mente subconscia sa che il piccolo mostro deve essere nutrito, bloccando tutto il resto. È la paura che impedisce alle persone di smettere, la paura di quella sensazione di vuoto e insicurezza che hanno quando smettono di inondare il loro cervello di dopamina. Solo perché non ne siete consapevoli non significa che non ci sia. Non dovete capirlo più di quanto un gatto debba capire dove sono i tubi dell'acqua calda, il gatto sa solo che se si siede in un certo punto sente caldo.

## Passività

La passività della nostra mente e la dipendenza dalle autorità che porta al lavaggio del cervello è la difficoltà principale nel rinunciare ai porno. La nostra educazione nella società, rafforzata dal lavaggio del cervello della nostra stessa dipendenza e delle influenze più potenti - i nostri amici, parenti e colleghi. La frase ’rinunciare’ è un classico esempio del lavaggio del cervello, poichè implica un autentico sacrificio. La splendida verità è che non c'è niente a cui rinunciare; al contrario, ti libererai da una terribile malattia e otterrai meravigliosi guadagni positivi. Cominceremo ora a rimuovere questo lavaggio del cervello, cominciando a non parlare più di ’rinunciare’ ma di smettere, femarsi o forse la vera posizione, **fuggire!**

L'unica cosa che ci persuade ad usare inizialmente sono le altre persone che li guardano, sentendo che ci stiamo perdendo qualcosa. Lavoriamo duramente per diventarne dipendenti, eppure non troviamo mai quello che ci siamo persi. Ogni volta che vediamo un'altra clip ci rassicuriamo che ci deve essere qualcosa, altrimenti la gente non li guarderebbe e il business non sarebbe così grande. Anche quando si è levato il vizio, l'ex-utente si sente privato quando si parla di una modella sexy, di un cantante o anche di una pornostar durante le feste o le funzioni sociali. *"Devono essere brave se tutti i miei amici ne parlano, giusto? Hanno foto gratis online?"* Si sentono sicuri, daranno solo una sbirciatina stasera e prima che se ne accorgano, sono di nuovo dipendenti.

Il lavaggio del cervello è estremamente potente e devi essere consapevole dei suoi effetti. La tecnologia continua a crescere e il futuro porterà siti e metodi di accesso esponenzialmente più veloci. L'industria del porno sta investendo milioni nella realtà virtuale in modo che diventi la prossima grande novità. Non sappiamo dove stiamo andando, disattrezzati per affrontare la tecnologia attuale o quella che verrà.

Stiamo per eliminare questo lavaggio del cervello, non è il non-utente che viene privato, ma l'utente che rinuncia a una vita intera di:

-   Salute

-   Energia

-   Ricchezza

-   Pace mentale

-   Sicurezza

-   Coraggio

-   Autostima

-   Felcità

-   Libertà

Che cosa guadagnano da questi considerevoli sacrifici? **ASSOLUTAMENTE NIENTE**, a parte l'illusione di cercare di tornare allo stato di pace, tranquillità e coraggio di cui gode sempre il non-utente.

## Alleviare i sintomi d’astinenza

Come spiegato in precedenza, gli utenti credono di guardare i porno per divertimento, relax o una sorta di educazione. La vera ragione è il sollievo dai sintomi di astinenza. La nostra mente subconscia comincia ad imparare che il porno su internet e la masturbazione in certi momenti tendono ad essere piacevoli. Man mano che diventiamo sempre più dipendenti dalla droga, maggiore diventa il bisogno di alleviare i sintomi di astinenza e la subdola trappola ci trascina sempre più in basso. Questo processo avviene così lentamente che non ne siamo nemmeno consapevoli, la maggior parte dei giovani utenti non si rendono conto di essere dipendenti fino a quando non tentano di smettere e anche allora, molti non lo ammettono.

Ecco un tipico colloquio avuto con centinaia di giovani:

>**Terapista:** "*Ti rendi conto che i porno su internet sono una droga e l'unica ragione per cui li guardi è che non riesci a smettere?*"
>
>**Paziente:** "*Storie! Mi piace, se non mi piacesse non lo farei!*"
>
>**Terapista:** "*Allora smetti per una settimana, solo per dimostrarmi che puoi farlo se vuoi.*"
>
>**Paziente:** "*Non ce n’è bisogno. Se volessi smettere, lo farei.*" 
>
>**Terapista:** "*Smetti per una settimana per provare a te stesso che non ne sei dipendente.*"
>
>**Paziente:** "*Perché farlo? Mi piace."*

Come già detto, gli utenti tendono ad alleviare i loro sintomi di astinenza in momenti di stress, noia, concentrazione o combinazioni di questi. Nei capitoli seguenti, ci occuperemo di questi aspetti del lavaggio del cervello.
